package com.pokedex.pokemon.model;

public class Generation {

    private String name;

    Generation() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

}
