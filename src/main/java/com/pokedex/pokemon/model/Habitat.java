package com.pokedex.pokemon.model;

public class Habitat {

    private String name;

    Habitat() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
