package com.pokedex.pokemon;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:pokedex.properties")
public class PropertiesSource {

}
