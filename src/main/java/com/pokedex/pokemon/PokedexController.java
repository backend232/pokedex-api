package com.pokedex.pokemon;

import com.pokedex.pokemon.model.Description;
import com.pokedex.pokemon.model.Evolutions;
import com.pokedex.pokemon.model.Pokemon;
import com.pokedex.pokemon.model.PokemonDetail;
import com.pokedex.pokemon.model.PokemonSpecies;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class PokedexController {

    @Value("${com.pokedexapi.pokemon}")
    private String urlPokemon;

    @Value("${com.pokedexapi.species}")
    private String urlSpecies;

    @Value("${com.pokedexapi.evolution-chain}")
    private String urlEvolutionChain;

    @GetMapping("/pokemonlist")
    private Pokemon getPokemonList(@RequestParam String offset, @RequestParam String limit) {
        RestTemplate restTemplate = new RestTemplate();
        Pokemon result = restTemplate.getForObject(urlPokemon, Pokemon.class, offset, limit);

        for (int i = 0; i < result.getResults().length; i++) {

            String url = result.getResults()[i].getUrl();

            PokemonDetail pokemonDetail = restTemplate.getForObject(url, PokemonDetail.class);
            result.getResults()[i] = pokemonDetail;
            result.getResults()[i].setUrl(url);
        }

        return result;
    }

    @GetMapping("/pokemon/{id}")
    private PokemonSpecies getPokemonById(@PathVariable String id) {

        RestTemplate restTemplate = new RestTemplate();
        PokemonSpecies result = new PokemonSpecies();
        Description description = restTemplate.getForObject(urlSpecies + id, Description.class);
        Evolutions evolutionChain = restTemplate.getForObject(urlEvolutionChain + id, Evolutions.class);
        PokemonDetail pokemonDetail = restTemplate.getForObject(urlPokemon + 1, PokemonDetail.class);

        result.setDescription(description);
        result.setEvolutions(evolutionChain);
        result.setPokemonDetail(pokemonDetail);

        return result;
    }

}
