package com.pokedex.pokemon.model;

public class PokemonSpecies {

    private PokemonDetail pokemonDetail;
    private Description description;
    private Evolutions evolutions;

    public PokemonSpecies() {

    }

    public void setPokemonDetail(PokemonDetail pokemonDetail) {
        this.pokemonDetail = pokemonDetail;
    }

    public PokemonDetail getPokemonDetail() {
        return this.pokemonDetail;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    public Description getDescription() {
        return this.description;
    }

    public void setEvolutions(Evolutions evolutions) {
        this.evolutions = evolutions;
    }

    public Evolutions getEvolutions() {
        return this.evolutions;
    }

}